<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Comment */
/* @var $form yii\widgets\ActiveForm */
/* @var $referrer string */
?>

<div class="form">
	<?php $form = ActiveForm::begin([
		'id' => 'post-form',
		'fieldConfig' => [
			'template' => '<div class="row">{label}{input}{error}</div>',
		],
		'enableClientValidation' => true,
	]); ?>

	<?= $form->errorSummary($model) ?>

	<?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

	<?= Html::input('hidden', 'referrer', $referrer) ?>

	<div class="form-group">
		<?= Html::submitButton('Отправить') ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
