<?php
/**
 * Project: comment
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */
/* @var $this yii\web\View */
/* @var $form app\models\Comment */

$this->title = 'Добавить комментарий';
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<div class="post-create">
	<?= $this->render('_form', [
		'model' => $form,
		'referrer' => $referrer,
	]) ?>
</div>
