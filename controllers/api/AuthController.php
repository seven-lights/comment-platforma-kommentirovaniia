<?php
/**
 * Project: Auth - Seven lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\controllers\api;

use app\models\IssuedAccessKeys;
use app\models\Site;

class AuthController extends \general\controllers\api\AuthController {
	public $layout = 'empty';
	public function actionGetAccessKey($secret_key, $service) {
		if(is_numeric($service)) {
			$site = empty($secret_key)
						? false
						: Site::findOne([
							'id' => $service,
							'secret' => $secret_key,
						]);
			if($site) {
				return $this->setIssuedAccessKey($service, 'open');
			} else {
				return $this->sendError(self::ERROR_ILLEGAL_SECRET_KEY);
			}
		} else {
			return parent::actionGetAccessKey($secret_key, $service);
		}
	}
	protected function setIssuedAccessKey($service, $access = 'close') {
		/** @var IssuedAccessKeys $model */
		if($model = IssuedAccessKeys::findOne( [ 'service' => $service ] )) {
			$model->access_key = \Yii::$app->security->generateRandomString(64);
			$model->expires_in = time() + \Yii::$app->params['api']['expires_on'];
			$model->access = $access;
		} else {
			$model = new IssuedAccessKeys();
			$model->service = $service;
			$model->access_key = \Yii::$app->security->generateRandomString(64);
			$model->expires_in = time() + \Yii::$app->params['api']['expires_on'];
			$model->access = $access;
		}
		if($model->save()) {
			return $this->sendSuccess([
				'access_key' => $model->access_key,
				'expires_in' => $model->expires_in,
			]);
		} else {
			return $this->sendError(self::ERROR_DB);
		}
	}
}