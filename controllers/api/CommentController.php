<?php

namespace app\controllers\api;

use app\extensions\ApiController;
use app\models\api\forms\CommentForm;
use app\models\api\search\CommentSearch;
use app\models\Comment;
use app\models\Domain;
use app\models\Page;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class CommentController extends ApiController {
	public $layout = 'emptyHtml';
	protected $_safe_actions = ['list-callback', 'list-frame', 'create-frame', 'delete-client', 'public-client'];

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['list', 'create', 'delete', 'list-close', 'create-close', 'delete-close'],
				'rules' => [
					[
						'actions' => ['list', 'create', 'delete'],
						'allow' => true,
						'matchCallback' => function ($rule, $action) {
							return $this->_access_key && $this->_access_key->access == 'open';
						}
					],
					[
						'actions' => ['list-close', 'create-close', 'delete-close'],
						'allow' => true,
						'matchCallback' => function ($rule, $action) {
							return $this->_access_key && $this->_access_key->access == 'close';
						}
					],
				],
			],
		];
	}
    public function actionListOpen() {
	    $data = (new CommentSearch())->search(\Yii::$app->request->get(), $this->_access_key->service);

	    if(is_int($data)) {
		    return $this->sendError($data);
	    }

	    if($data === []) {
		    return $this->sendError(self::ERROR_DB);
	    }

	    return $this->sendSuccess($data);
    }
    public function actionCreateOpen() {
	    if(\Yii::$app->request->isGet) {
		    return $this->sendError(self::ERROR_ILLEGAL_REQUEST_METHOD);
	    }
	    $form = new CommentForm();

	    if(!$form->load(\Yii::$app->request->post())) {
		    return $this->sendError(self::ERROR_NO_DATA);
	    }

	    if($form->validate()) {
		    try {
			    if(!$page = Page::findOne(['path' => $form->page, 'site_id' => $this->_access_key->service])) {
				    $page = new Page();
				    $page->path = $form->page;
				    $page->site_id = $this->_access_key->service;
				    if(!$page->save()) {
					    throw new Exception('Ошибка создания страницы');
				    }
			    }

			    $model = new Comment();
			    $model->page_id = $page->id;
			    $model->text = $form->text;
			    $model->answer_to = $form->answer_to;
			    $model->is_public = $form->is_public;
			    $model->ip = Yii::$app->request->getUserIP();
			    $model->browser = 'API';
			    $model->save(false);

		    } catch(Exception $e) {
			    return $this->sendError(self::ERROR_DB);
		    }
		    return $this->sendSuccess([
			    'comment' => array_merge(
				    $model->getAttributes(['id', 'text', 'answer_to', 'is_public']),
				    ['page' => $model->page->path],
				    ['username' => empty($model->user) ? 'Анонимно' : $model->user->nick]
			    )
		    ]);
	    } else {
		    $errors = $this->getErrorCodes([
			    'text' => self::ERROR_ILLEGAL_COMMENT,
			    'page' => self::ERROR_ILLEGAL_COMMENT_PAGE_PATH,
			    'answer_to' => self::ERROR_ILLEGAL_COMMENT_ANSWER_TO,
			    'is_public' => self::ERROR_ILLEGAL_COMMENT_IS_PUBLIC,
		    ], $form);
	    }

	    if(!isset($errors)) {
		    $errors = self::ERROR_UNKNOWN;
	    }
	    return $this->sendError($errors);
    }
    public function actionDeleteOpen($id) {
	    $model = Comment::find()
		    ->where(['id' => $id, 'page.site_id' => $this->_access_key->service])
		    ->innerJoin('page')
	        ->one();
	    /* @var Comment $model */
	    if($model) {
		    if($model->delete()) {
			    return $this->sendSuccess([]);
		    } else {
			    return $this->sendError(self::ERROR_DB);
		    }
	    } else {
		    return $this->sendError(self::ERROR_NO_COMMENT);
	    }
    }

	public function actionListClose($site) {
		$data = (new CommentSearch())->search(\Yii::$app->request->get(), $site);

		if(is_int($data)) {
			return $this->sendError($data);
		}

		if($data === []) {
			return $this->sendError(self::ERROR_DB);
		}

		return $this->sendSuccess($data);
	}
	public function actionCreateClose($site) {
		if(\Yii::$app->request->isGet) {
			return $this->sendError(self::ERROR_ILLEGAL_REQUEST_METHOD);
		}
		$form = new CommentForm();

		if(!$form->load(\Yii::$app->request->post())) {
			return $this->sendError(self::ERROR_NO_DATA);
		}

		if($form->validate()) {
			try {
				if(!$page = Page::findOne(['path' => $form->page, 'site_id' => $site])) {
					$page = new Page();
					$page->path = $form->page;
					$page->site_id = $site;
					if(!$page->save()) {
						throw new Exception('Ошибка создания страницы');
					}
				}

				$model = new Comment();
				$model->page_id = $page->id;
				$model->text = $form->text;
				$model->answer_to = $form->answer_to;
				$model->is_public = $form->is_public;
				$model->ip = Yii::$app->request->getUserIP();
				$model->browser = 'API';
				$model->save(false);

			} catch(Exception $e) {
				return $this->sendError(self::ERROR_DB);
			}
			return $this->sendSuccess([
				'comment' => array_merge(
					$model->getAttributes(['id', 'text', 'answer_to', 'is_public']),
					['page' => $model->page->path],
					['username' => empty($model->user) ? 'Анонимно' : $model->user->nick]
				)
			]);
		} else {
			$errors = $this->getErrorCodes([
				'text' => self::ERROR_ILLEGAL_COMMENT,
				'page' => self::ERROR_ILLEGAL_COMMENT_PAGE_PATH,
				'answer_to' => self::ERROR_ILLEGAL_COMMENT_ANSWER_TO,
				'is_public' => self::ERROR_ILLEGAL_COMMENT_IS_PUBLIC,
			], $form);
		}

		if(!isset($errors)) {
			$errors = self::ERROR_UNKNOWN;
		}
		return $this->sendError($errors);
	}
	public function actionDeleteClose($id, $site) {
		$model = Comment::find()
			->where(['id' => $id, 'page.site_id' => $site])
			->innerJoin('page')
			->one();
		/* @var Comment $model */
		if($model) {
			if($model->delete()) {
				return $this->sendSuccess([]);
			} else {
				return $this->sendError(self::ERROR_DB);
			}
		} else {
			return $this->sendError(self::ERROR_NO_COMMENT);
		}
	}

	public function actionListCallback() {
		Yii::$app->response->format = Response::FORMAT_JSONP;
		$data = Comment::getStructuredComments(parse_url($this->getReferrer()));
		if(is_null($data)) {
			return [
				'callback' => 'sl_comment',
				'data' => false,
			];
		}
		return [
			'callback' => 'sl_comment',
			'data' => $data,
		];
	}
	public function actionListFrame() {
		$url = parse_url($this->getReferrer());
		$data = Comment::getStructuredComments($url);
		if(is_null($data)) {
			return 'Ошибка';
		}
		$form = new Comment();
		$form->loadDefaultValues();

		if(Yii::$app->request->isPost) {
			$domain = Domain::findOne(['domain' => $url['host']]);
			if(Comment::createComment($url, $domain, $form)) {
				return $this->redirect(['list-frame', 'r' => 'ok']);
			}
		}

		return $this->render('list-frame', [
			'data' => $data,
			'form' => $form,
			'r' => Yii::$app->request->get('r'),
			'referrer' => $this->getReferrer(),
		]);
	}
	public function actionCreateFrame() {
		if($url = parse_url($this->getReferrer())) {
			if(!empty($url['host'])
				&& $domain = Domain::findOne(['domain' => $url['host']])) {

				$form = new Comment();
				$form->loadDefaultValues();

				if(Yii::$app->request->isPost) {
					if(Comment::createComment($url, $domain, $form)) {
						return $this->redirect(['create-frame', 'r' => 'ok']);
					}
				}
				return $this->render('create-frame', [
					'form' => $form,
					'r' => Yii::$app->request->get('r'),
					'referrer' => $this->getReferrer(),
				]);
			}
		}
		return 'Ошибка';
	}

	public function actionBootstrap() {
		Yii::$app->response->format = Response::FORMAT_JSONP;
		$data = Comment::getStructuredComments(parse_url($this->getReferrer()));
		if(is_null($data)) {
			return [
				'callback' => 'sl_comment',
				'data' => false,
			];
		}
		return [
			'callback' => 'sl_comment',
			'data' => $data,
		];
	}

	public function actionDelete($id) {
		Yii::$app->response->format = Response::FORMAT_JSONP;
		/* @var $model Comment */
		$model = Comment::findOne($id);
		if(self::isAdmin($model->page->site)){
			if($model->delete() !== false) {
				return [
					'callback' => 'sl_comment_delete',
					'data' => true,
				];
			}
		}
		return [
			'callback' => 'sl_comment_delete',
			'data' => false,
		];
	}
	public function actionPublic($id) {
		Yii::$app->response->format = Response::FORMAT_JSONP;
		/* @var $model Comment */
		$model = Comment::findOne($id);
		if(self::isAdmin($model->page->site)){
			$model->is_public = true;
			if($model->save()) {
				return [
					'callback' => 'sl_comment_public',
					'data' => true,
				];
			}
		}
		return [
			'callback' => 'sl_comment_public',
			'data' => false,
		];
	}

	private function getReferrer() {
		if(Yii::$app->request->post('referrer')) {
			return Yii::$app->request->post('referrer');
		}
		return Yii::$app->request->referrer;
	}
}