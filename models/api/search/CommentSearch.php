<?php

namespace app\models\api\search;

use app\models\Comment;
use Yii;
use yii\base\Model;

/**
 * ProductSearch represents the model behind the search form about `app\models\Product`.
 */
class CommentSearch extends Comment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
	        ['page', 'string'],
	        ['is_public', 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 * @param $site_id
	 * @return array
	 */
    public function search($params, $site_id) {
	    $query = Comment::find()
		    ->select(['id', 'username', 'text', 'answer_to', 'is_public'])
	        ->where(['page.site_id' => $site_id])
	        ->innerJoin('page');

	    // load the search form data and validate
	    if ($this->load($params) && $this->validate()) {
		    $query->andFilterWhere([
			    'id' => $this->id,
			    'page.path' => $this->page,
			    'is_public' => $this->is_public,
		    ]);
	    }

	    $comments = [];
	    foreach ($query->all() as $val) {
		    /* @var $val Comment */
		    $comments[] = array_merge(
			    $val->getAttributes(['id', 'text', 'answer_to', 'is_public']),
			    ['page' => $val->page->path],
			    ['username' => empty($val->user) ? 'Анонимно' : $val->user->nick]
		    );
	    }

	    return [
		    'comments' => $comments,
	    ];
    }
}
