<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "domain".
 *
 * @property string $domain
 * @property integer $site_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Site $site
 */
class Domain extends ActiveRecord
{
	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'domain';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['domain', 'site_id'], 'required'],
            ['site_id', 'integer'],
            ['domain', 'string', 'max' => 100],
	        ['domain', 'match', 'pattern' => '#^[a-z]+(\.[a-z]+)+$#i'],
            ['domain', 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'domain' => 'Domain',
            'site_id' => 'Site ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }
}
