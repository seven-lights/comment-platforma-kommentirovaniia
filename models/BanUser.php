<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ban_user".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $site_id
 * @property integer $owner_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $owner
 * @property Site $site
 * @property User $user
 */
class BanUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ban_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'site_id', 'created_at', 'updated_at'], 'required'],
            [['user_id', 'site_id', 'owner_id', 'created_at', 'updated_at'], 'integer'],
            [['user_id', 'site_id'], 'unique', 'targetAttribute' => ['user_id', 'site_id'], 'message' => 'The combination of User ID and Site ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'site_id' => 'Site ID',
            'owner_id' => 'Owner ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
