<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $path
 * @property integer $site_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Comment[] $comments
 * @property Site $site
 */
class Page extends ActiveRecord
{
	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['path', 'site_id'], 'required'],
            ['site_id', 'integer'],
            ['path', 'string', 'max' => 255],
            [['path', 'site_id'], 'unique', 'targetAttribute' => ['path', 'site_id'], 'message' => 'The combination of Path and Site ID has already been taken.'],
	        ['site_id', 'exist', 'targetClass' => Site::className(), 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
            'site_id' => 'Site ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }
}
