<?php

use yii\db\Schema;
use yii\db\Migration;

class m150413_190519_create_site_table extends Migration
{
    public function up()
    {
	    $this->createTable('site', [
		    'id' => Schema::TYPE_PK,
		    'name' => Schema::TYPE_STRING . ' NOT NULL',
		    'enable_anonym_comment' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 1',
		    'get_param' => Schema::TYPE_STRING . ' NOT NULL',
		    'pre_moderation' => Schema::TYPE_SMALLINT . '(2) NOT NULL',
		    'enable_tree_view' => Schema::TYPE_BOOLEAN . ' NOT NULL',
		    'secret' => 'CHAR(30)',
		    'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->addForeignKey('user_id_FK_site', 'site', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m150413_190519_create_site_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
